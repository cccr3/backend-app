FROM amazoncorretto:11

RUN yum install -y glibc-langpack-ko
ENV LANG ko_KR.UTF8
ENV LC_ALL ko_KR.UTF8
RUN ln -sf /usr/share/zoneinfo/Asia/Seoul /etc/localtime
VOLUME /tmp
ARG JAR_FILE
COPY build/libs/usertraders-back-0.0.2.jar app.jar

ENTRYPOINT ["java", \
    "-jar", \
    "/app.jar"]
